    function locReload() {
	    $(".categories, .slider").removeClass("hidden");
	    $(".foodlist, .foodlistSelect, .foodlistAjax, .bg-picture").addClass("hidden");
	    //location.reload(); 
    }
    function donothing() {
	    //
    }
    function resetBasket() {
		var url_to_sum_load = "http://www.delfino.pizza/userdata/app.php?action=getBasketSum&uuid=" + device.uuid;
	    $(".shopping_bag .sum-in-cart").load(encodeURI(url_to_sum_load), function(event) { }); 
	    var url_to_load = "http://www.delfino.pizza/userdata/app.php?action=get_basket_lines&uuid=" + device.uuid;
	    $(".shopping_bag .ajaxBasketInner").load(url_to_load, function(event) { }); 
    }
	$(document).ready(function(){ 
		
		$("body").on("click", ".next-order", function(event){
		    event.preventDefault(); 
		    $("#ajaxAddress").load("http://www.delfino.pizza/userdata/app.php?action=getAddress&uuid=" + device.uuid, function () {
			    $("#f_payment2").prop("checked",false);
			    $("#f_payment1").prop("checked",true);
		    });     
	    });  
	     
		$("body").on("click", "#sendTotalOrder", function(event){
		    event.preventDefault(); 
		    var check_message = 'Bitte vervollständigen Sie alle Angaben:';
		    var check_fields = true; // true
		    
		    $("#form-bestellung input").each( function(event){
			    if ($(this).val() == '') {
				    check_message = check_message + "\n" + $(this).data("text");
				    check_fields = false;
			    }
		    });
		    
		    
		    if (check_fields == true) { 
			    var totalOrder = 'http://www.delfino.pizza/userdata/app.php?action=send_order&'+$("#form-bestellung").serialize();
			    $("#ajaxSend").load(totalOrder, function(event) {
				    var sum_for_paypal = $(".block-price.gesamtsumme").text();
				    sum_for_paypal = sum_for_paypal.replace("€","");
				    sum_for_paypal = sum_for_paypal.replace(" ","");
				    sum_for_paypal = sum_for_paypal.replace(",",".");
				    sum_for_paypal = parseFloat(sum_for_paypal); 
					$("input[name*='os0']").prop("value",sum_for_paypal);
			    }); 
			    
			    //INIT of WK fields and guthaben
			    $(".account .euro").load("http://www.delfino.pizza/userdata/app.php?action=getUser&uuid=" + device.uuid + "&data="+encodeURI(JSON.stringify(device)));
				var payment = $('#form-bestellung input[name*="f_payment"]:checked').val();
				var payment_text = 'Zahlungsart: Barzahlung\n\nBitte halten Sie den Gesamtbetrag möglichst passend bereit. Danke!';
				if (payment == 2) {
					payment_text = 'Zahlungsart: per PayPal\n\nBitte weiteren Anweisungen folgen.';
				}
			    
			    
			    navigator.notification.alert(
				    payment_text,  // message
				    resetBasket,         // callback
				    'Bestellung übertragen',            // title
				    'OK'                  // buttonName
				); 
		    
		    } else {
			    navigator.notification.alert(
				    check_message,  // message
				    donothing,         // callback
				    'Fehlende Angaben',            // title
				    'OK'                  // buttonName
				); 
		    }
		    //load("http://www.delfino.pizza/userdata/app.php?action=getAddress&uuid=" + device.uuid, function () { 
			//
		    //});     
	    }); 
	    
	    
	    
		$("body").on("blur", "#orderModal input", function(event){
			var fieldVal = $(this).val();
			var urlToLoad = "http://www.delfino.pizza/userdata/app.php?action=setAddress&uuid=" + device.uuid + "&table="+$(this).data("table")+"&field="+$(this).data("field")+"&value="+fieldVal;
		    $("#ajaxAddressResponse").load(encodeURI(urlToLoad));
		    //alert(urlToLoad);
	    });  
		$("body").on("blur", "#orderModal textarea", function(event){
			var fieldVal = $(this).text();
			var urlToLoad = "http://www.delfino.pizza/userdata/app.php?action=setAddress&uuid=" + device.uuid + "&table="+$(this).data("table")+"&field="+$(this).data("field")+"&value="+fieldVal;
		    $("#ajaxAddressResponse").load(encodeURI(urlToLoad));
		    //alert($(this).urlToLoad());
	    });  

		
	    $(".a-liefern").click(function(){
  			$(".foodlist").load("http://www.delfino.pizza/userdata/app.php?action=getFoodlist");
  			$(".foodlistSelect").removeClass("hidden");
  			//$(".foodlist").removeClass("hidden");
  			$(".categories, .slider").addClass("hidden");
  			$(".bg-picture img").addClass("hidden");
  			$(".bg-picture").removeClass("hidden").find("img.zum_liefern").removeClass("hidden"); 
	    });
	    $(".a-abholen").click(function(){
  			$(".foodlist").load("http://www.delfino.pizza/userdata/app.php?action=getFoodlist");
  			$(".foodlist").removeClass("hidden");
  			$(".categories, .slider").addClass("hidden"); 
  			
  			$(".bg-picture img").addClass("hidden");
  			$(".bg-picture").removeClass("hidden").find("img.gerichte").removeClass("hidden"); 
	    });
	    
	    //$("menu_button").click(function(){
		    //$(".menu").removeClass("close");
		    
	    //});
	    
	    $(".foodlist").on("click", ".food_item", function(event){
		   event.preventDefault();
		   $(this).next().toggleClass("closed"); 
		   $(this).find(".glyphicon").toggleClass("hidden");
			    
	    });  
	    //$("#meinModal").modal(); 
	    
	    
	    $(".a-bestellungen").on("click", function(event){
	    	$(".foodlistSelect, .foodlist, .categories").addClass("hidden"); 
		    
    	    var url_to_load = "http://www.delfino.pizza/userdata/app.php?action=getMyOrders&uuid=" + device.uuid; 
			$(".favoriten").load(encodeURI(url_to_load), function(event) { }); 
				
	    	$("#menuModal .close").click();
	    });
	    $(".menu-close").on("click", function(event){
	    	$(".favoriten").html('');
	    	$("#menuModal .close").click();
	    });
	    $(".a-liefern").on("click", function(event){
	    	$(".foodlist").addClass("hidden");
	    });
	    $(".a-abholen").on("click", function(event){
	    	$(".foodlistSelect").addClass("hidden");
	    });
	    $(".menu-a").on("click", function(event){
		    var sel = $(this).attr("data-selector");
		    var tar = $(this).attr("data-href"); 
		    //alert(sel+' // '+tar);
		    $(sel).removeClass("hidden").load(tar); 
		    $(".foodlistAjax, .foodlist, .foodlistSelect, .categories").addClass("hidden");
	    });
	    
	    $(".shopping_bag .toggle").on("click", function(event) {
		    $(this).parents(".shopping_bag").toggleClass("opened").find(".toggle").toggleClass("hidden");
	    }); 
	    
	    $(".foodlistSelect div, .a-abholen").on("click", function(){
		    event.preventDefault();
		    $(".bg-picture img").addClass("hidden");
		    $(".bg-picture img.gerichte").removeClass("hidden");
  	     
		  	var url_to_load = "http://www.delfino.pizza/userdata/app.php?action=setShipping&uuid=" + device.uuid + "&add_plz="+$(this).attr("value")+"&add_cost="+$(this).attr("data-kosten")+"&add_min="+$(this).attr("data-min")+"&add_free="+$(this).attr("data-free");
	  	    $(".shopping_bag .ajaxBasketInner").load(url_to_load, function(event) { 
	    	    $(".foodlistSelect").addClass("hidden");
	    	    $(".foodlist").removeClass("hidden");
	    	    
	    	    var url_to_sum_load = "http://www.delfino.pizza/userdata/app.php?action=getBasketSum&uuid=" + device.uuid; 
				$(".shopping_bag .sum-in-cart").load(encodeURI(url_to_sum_load), function(event3) { }); 
	  	    });
	    });
	    
	    
	    
	      /*
	        data-addgroup
	        data-addartnr
	        data-addgericht
	        data-addpricename
	        data-addpricename2
	        data-addpricename3
	        data-addprice
	        data-addprice2
	        data-addprice3
	      */
	    $(".foodlist").on("click", ".sub_food_item.direct", function(event1){
	  	    event1.preventDefault();
	  	    var url_to_load = "http://www.delfino.pizza/userdata/app.php?action=setItem&uuid=" + device.uuid + "&add_group="+$(this).attr("data-addgroup") + "&add_artnr="+$(this).attr("data-addartnr") + "&add_gericht="+$(this).attr("data-addgericht") + "&add_price_name="+$(this).attr("data-addpricename") + "&add_price="+$(this).attr("data-addprice");
	  		$(".shopping_bag .ajaxBasketInner").load(encodeURI(url_to_load), function(event) {
		  		var url_to_sum_load = "http://www.delfino.pizza/userdata/app.php?action=getBasketSum&uuid=" + device.uuid;
			    $(".shopping_bag .sum-in-cart").load(encodeURI(url_to_sum_load), function(event) { }); 
			    //alert(url_to_sum_load); 
	  		}); 
			  
		    
	    });
	    
	    $(".foodlist").on("click", ".sub_food_item.indirect", function(event4){
	  	    event4.preventDefault();
	  	    var url_to_pizza_load = "http://www.delfino.pizza/userdata/app.php?action=getPizzaModal&uuid=" + device.uuid + "&add_group="+$(this).attr("data-addgroup") + "&add_artnr="+$(this).attr("data-addartnr") + "&add_gericht="+$(this).attr("data-addgericht") + "&add_price_name="+$(this).attr("data-addpricename") + "&add_price="+$(this).attr("data-addprice") + "&add_price_name2="+$(this).attr("data-addpricename2") + "&add_price2="+$(this).attr("data-addprice2") + "&add_price_name3="+$(this).attr("data-addpricename3") + "&add_price3="+$(this).attr("data-addprice3");
		    //var url_to_pizza_load = "http://www.delfino.pizza/userdata/app.php?action=getPizzaModal&uuid=" + device.uuid;
	  	    $("h4.modal-title").html($(this).find(".col-xs-8.pad-left").html());
		    //alert('MODAL');
		    $("#meinModal .modal-body").load(encodeURI(url_to_pizza_load), function(event5) { });  
	    }); 
	    
	    
	    $(".btn-default .glyphicon-trash").on("click", function(event){
  	    
  	    	$("a.next-order").css("display","none");
	  	    $(".shopping_bag").removeClass("opened").find(".ajaxBasketInner").html('');
	  	    $(".shopping_bag .glyphicon-menu-down").removeClass("hidden").next().addClass("hidden");
	        $('body').animate({
	            scrollTop: 0
	        }, 500);
	  	    $(".shopping_bag .ajaxBasketInner").load(encodeURI("http://www.delfino.pizza/userdata/app.php?action=deleteBasket&uuid=" + device.uuid), function(event2) {
    	  $(".shopping_bag .sum-in-cart").html('0,00&nbsp;€');
          setTimeout(locReload, 500);
    	    
    	  }); 
    	  
	    });
	    $(".bg-red .logo").on("click", function(event){
  	    
  	    	$(".favoriten").html('');
	  	    $(".shopping_bag").removeClass("opened");
	  	    $(".shopping_bag .glyphicon-menu-down").removeClass("hidden").next().addClass("hidden");
	        $('body').animate({
	            scrollTop: 0
	        }, 500);
	  	    
			setTimeout(locReload, 500);  
    	  
	    });
	    
	    
	    $("body").on("click", ".radio.pizza", function(event){
  	    	$('.pizza-to-basket, .extra-zutaten.hidden').removeClass("hidden").removeClass("hidden_");
  	    	$('.extra-zutaten').find("span.price").addClass("hidden");
  	    	$('.extra-zutaten').find("span.price."+$(this).attr("data-span")).removeClass("hidden"); 
	    });
	     
	    $("body").on("click", ".btn-default.pizza-to-basket", function(event){ 
  	    	
  	    	var price_name = $(this).parents(".modal-content").find('input[name=PizzaGroesse]:checked').next().find(".just-size").text(); 
  	    	var price_euro = $(this).parents(".modal-content").find('input[name=PizzaGroesse]:checked').next().next().text();
  	    	price_euro = parseFloat(price_euro.replace(',', '.'));
  	    	
  	    	var url_to_parse_orig = decodeURI($("#ajaxHidden").html());
  	    	//alert(encodeURI(url_to_parse_orig));
  	    	var url_to_parse = url_to_parse_orig;
  	    	url_to_parse = url_to_parse.replace('PPPNAME', price_name);
  	    	url_to_parse = url_to_parse.replace('PPPPRICE', price_euro); //[amp; 
  	    	
  	    	
	  		$(".shopping_bag .ajaxBasketInner").load(encodeURI(url_to_parse), function(event) {
		  		
		  		
		  		var extras_sum = 0.00;
		  		var extras_dazu = '';
		  		
		  		$("body .modal-content").find(".extra-zutaten .food_option .checkbox input:checked").each(function(event) {
			  		//$(this).remove();
			  		extras_dazu = extras_dazu + $(this).next().text() + ' (' + $(this).parent().find("span.price:not(.hidden)").text() + ' ), ';
			  		extras_sum = extras_sum + parseFloat($(this).parent().find("span.price:not(.hidden)").attr("data-value"));
		  		});
		  		
		  		if (extras_dazu != '') {
			  		//alert(extras_dazu);
		  	    	url_to_parse_orig = url_to_parse_orig.replace('add_group=Pizza', 'add_group=dazu');
		  	    	url_to_parse_orig = url_to_parse_orig.replace('PPPNAME', '');
		  	    	url_to_parse_orig = url_to_parse_orig.replace('PPPPRICE', extras_sum);
		  	    	url_to_parse_orig = url_to_parse_orig + '&gericht_dazu='+extras_dazu;
			  		
			  		//alert(encodeURI(url_to_parse_orig));
			  		$(".shopping_bag .ajaxBasketInner").load(encodeURI(url_to_parse_orig), function(event) {
		  	    		var url_to_sum_load = "http://www.delfino.pizza/userdata/app.php?action=getBasketSum&uuid="+ device.uuid;
				  		$(".shopping_bag .sum-in-cart").load(encodeURI(url_to_sum_load), function(event7) { }); 
			  		}); 
	  	    	} else {
		  	    	//alert('keine extras_dazu');
		  	    	var url_to_sum_load = "http://www.delfino.pizza/userdata/app.php?action=getBasketSum&uuid="+ device.uuid;
		  			$(".shopping_bag .sum-in-cart").load(encodeURI(url_to_sum_load), function(event7) { }); 
		  			
	  	    	}
	  	    	
		  		
	  		}); 
	  		$('#meinModal').modal('hide');
	  		
 
  	    	
	    });
  		$('#meinModal').on('hidden.bs.modal', function () {
		    $(".btn-default.pizza-to-basket").addClass("hidden_");
		});
	    
	}); 
	
	
	document.addEventListener("deviceready", onDeviceReady, false);
	function onDeviceReady() { 
	    //$(".account .euro").load("http://www.delfino.pizza/userdata/app.php?action=getUser&uuid=" + device.uuid + "&data=" + JSON.stringify(device));
	    //console.log(device.cordova);
	     
			//alert(device.uuid);
			
			$("input[name*='custom'], input#BenutzerID").prop("value",device.uuid);
		    $(".account .euro").load("http://www.delfino.pizza/userdata/app.php?action=getUser&uuid=" + device.uuid + "&data="+encodeURI(JSON.stringify(device)));
		    
			var url_to_sum_load = "http://www.delfino.pizza/userdata/app.php?action=getBasketSum&uuid=" + device.uuid;
		    $(".shopping_bag .sum-in-cart").load(encodeURI(url_to_sum_load), function(event) { }); 
		    var url_to_load = "http://www.delfino.pizza/userdata/app.php?action=get_basket_lines&uuid=" + device.uuid;
		    $(".shopping_bag .ajaxBasketInner").load(url_to_load, function(event) { }); 
		    
		    $('body').animate({
	            scrollTop: 0
	        }, 0);
	} 
    app.initialize();